/*
 * (C) 2011 Dominick Vanthienen, dominick.vanthienen@mech.kuleuven.be, Department of Mechanical
 Engineering, Katholieke Universiteit Leuven, Belgium.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 Alternatively, the contents of this file may be used under the terms of
 either of the New BSD License
 */
#include "tdFromForceGenerator.hpp"
#include <rtt/os/TimeService.hpp>
#include <iostream>
#include <ocl/Component.hpp>
ORO_CREATE_COMPONENT( trajectory_generators::tdFromForceGenerator );

namespace trajectory_generators {

using namespace KDL;
using namespace RTT;

tdFromForceGenerator::tdFromForceGenerator(const std::string& name) :
	TaskContext(name, PreOperational),
    pos_des_local(Frame::Identity()),
    vel_des_local(Twist::Zero()),
    x(0),
    y(0),
    z(0),
    omegax(0),
    omegay(0),
    omegaz(0),
    tau(0.5),
    alpha(0),
    filter(false),
    Jqvec(120,0.0),
    metadata("{ports={"
    "DesiredVelocity={type='trajectory_generator', geom_sem='TwistCoordinateSemantics(o2,o2,o1,o1)'},"
    "}}")

{
	this->properties()->addProperty("tau",tau_prop).doc("low pass filter time constant");
	this->properties()->addProperty("filter",filter).doc("low pass filter on = true");
	this->properties()->addProperty("nq",nq).doc("number of joints");
	this->addProperty("available_joints",available_joints);
        this->addProperty("joint_names",joint_names);
	this->addProperty("names_o1",names_o1);
	this->addProperty("names_o2",names_o2);
	this->addProperty("chain_joints",chain_joints);
	this->addProperty("chain_joints_alph",chain_joints_alph);
        this->addProperty("joints_force_feedback",joints_force_feedback);
        this->addProperty("joints_force_feedback_alph",joints_force_feedback_alph);
        this->addProperty("Jq",Jq);
        this->addProperty("Jq_select",Jq_select);
        this->addProperty("e_kdl",e_kdl);
        this->addProperty("e_std",e_std);
        this->addProperty("vel_des_local",vel_des_local);
        this->addProperty("metadata",metadata).doc("Meta-data of the component");

	this->ports()->addPort("DesiredPosition", pos_des);
	this->ports()->addPort("DesiredVelocity", vel_des);
        this->ports()->addPort("joint_names",joint_names_port).doc("all available joint names");
	this->ports()->addPort("deltaqdot", deltaqdot_port).doc("error on control signals = difference in joint velocities");
	this->ports()->addEventPort("Jq_w", Jq_port);

	this->ports()->addPort("Jqvec", Jqvec_port);

  std::cout << "Td_from_force_generator constructed !" <<std::endl;
}

bool tdFromForceGenerator::configureHook() {
	tau = tau_prop;

	pos_des.write(pos_des_local);
	vel_des.write(vel_des_local);

        joint_names_port.read(joint_names);

        // Allocate safe amount of memory
	e_kdl.resize(joint_names.size());
	KDL::SetToZero(e_kdl);
        Jq_select.data.resize(joint_names.size(),joint_names.size());
        Jq_select.data.setZero();
	std::cout << "Td_from_force_generator configured !" <<std::endl;

        return true;
}

bool tdFromForceGenerator::startHook() {
	pos_des.write(pos_des_local);
	vel_des.write(vel_des_local);

        joint_names_port.read(joint_names);
        checkJoints();
        arrangeJoints();

        // Allocate correct amount of memory
        e_kdl.resize(nq);
        KDL::SetToZero(e_kdl);
        Jq_select.data.resize(6,nq);
	Jq_select.data.setZero();
	std::cout << "Td_from_force_generator started !" <<std::endl;

        return true;
}

void tdFromForceGenerator::updateHook()
{
	//SetToZero(vel_des_local);
	//vel_des.write(vel_des_local);
        //return;
	tau = tau_prop;
	//read e en Jq
	if((NoData != deltaqdot_port.read(e_std)) && (NoData != Jq_port.read(Jq)))
	{
		for(int i=0; i<6;i++){
			for(int j=0; j<nq; j++){
				Jqvec[i*nq+j]=Jq(i,j);
			}
		}
		Jqvec_port.write(Jqvec);

	        //Arrange deltaqdot in the order of the Jacobian (minus-sign is due to different definition of the error)
		for (int index = 0; index<nq; index++){
			if (names_o1.size()>names_o2.size()){
                	e_kdl(index) = -e_std[findJointIndex(joints_force_feedback_alph[index], names_o1)];
			}
			if (names_o2.size()>names_o1.size()){
                        e_kdl(index) = -e_std[findJointIndex(joints_force_feedback_alph[index], names_o2)];
                        }
		}

                //Set Jacobian: only the joints that contribute
		for (int index = 0; index<nq; index++){
		Jq_select.setColumn(index,Jq.getColumn(findJointIndex(joints_force_feedback_alph[index], joint_names)));
		}

		//calculate twist
		MultiplyJacobian(Jq_select, e_kdl, vel_des_local);

		//filter
		if(filter)
		{
			delta_time = os::TimeService::Instance()->secondsSince(previous_time);
			previous_time = os::TimeService::Instance()->getTicks();
			alpha = delta_time/(tau+delta_time);

			x = x + alpha*(vel_des_local(0)-x);
			y = y + alpha*(vel_des_local(1)-y);
			z = z + alpha*(vel_des_local(2)-z);

			omegax = omegax +alpha*(vel_des_local(3)-omegax);
			omegay = omegay +alpha*(vel_des_local(4)-omegay);
			omegaz = omegaz +alpha*(vel_des_local(5)-omegaz);

			//put it back in a twist
			vel_des_local(0) = x;
			vel_des_local(1) = y;
			vel_des_local(2) = z;
			vel_des_local(3) = omegax;
			vel_des_local(4) = omegay;
			vel_des_local(5) = omegaz;
		}

		//put it on a port
		vel_des.write(vel_des_local);
	}
}

void tdFromForceGenerator::stopHook() {
    KDL::SetToZero(vel_des_local);
	vel_des.write(vel_des_local);

    std::cout << "Td_from_force_generator stopped!" <<std::endl;


}

bool tdFromForceGenerator::checkJoints() {

  // Construct a vector with the joints used for force control,  by substracting both chains from eachother
  if (names_o1.size()>names_o2.size()){
    chain_joints.resize(names_o1.size()-names_o2.size());
    for (int i = 0; i<chain_joints.size(); i++){
      chain_joints[i] = names_o1[i+names_o2.size()];
    }
  }
  if (names_o1.size()<names_o2.size()){
    chain_joints.resize(names_o2.size()-names_o1.size());
    for (int i = 0; i<chain_joints.size(); i++){
      chain_joints[i] = names_o2[i+names_o1.size()];
    }
  }
  if (names_o1.size()==names_o2.size()){
    log(Error) << "No joints available for force control" << endlog();
  }

  // Check if the obtained joints are allowed to be used for force feedback

  for (int i = 0; i<chain_joints.size(); i++){
    foundFlag = false;
    for (int j = 0; j<available_joints.size(); j++){
      if (chain_joints[i] == available_joints[j]) {
        joints_force_feedback.push_back(chain_joints[i]);
        foundFlag = true;
      }
    }
  }
  nq = joints_force_feedback.size();
  return true;
}

int tdFromForceGenerator:: findJointIndex(const std::string& string, const std::vector<std::string>& table) {
  int index;
  for (int i = 0; i<table.size(); i++) {
    if (string == table[i]){
      index = i;
      return index;
    }
  }
  log(Error) << "Joint is not present in the table" << endlog();
  return -1;
}

void tdFromForceGenerator:: arrangeJoints() {
  chain_joints_alph.resize(chain_joints.size());
  int k = 0;
  for (int i = 0; i<joint_names.size(); i++) {
    for (int j = 0; j<chain_joints.size(); j++) {
      if (chain_joints[j] == joint_names[i] ) {
        chain_joints_alph[k] = chain_joints[j];
        k++;
      }
    }
  }
  joints_force_feedback_alph.resize(joints_force_feedback.size());
  int l = 0;
  for (int i = 0; i<joint_names.size(); i++) {
    for (int j = 0; j<joints_force_feedback.size(); j++) {
      if (joints_force_feedback[j] == joint_names[i] ) {
        joints_force_feedback_alph[l] = joints_force_feedback[j];
        l++;
      }
    }
  }
}

}//namespace
