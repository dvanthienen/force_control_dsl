/*
 * (C) 2011 Dominick Vanthienen, dominick.vanthienen@mech.kuleuven.be, Department of Mechanical
 Engineering, Katholieke Universiteit Leuven, Belgium.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 Alternatively, the contents of this file may be used under the terms of
 either of the New BSD License
 */
#include <kdl/velocityprofile_trap.hpp>
#include <kdl/jacobian.hpp>
#include <kdl/jntarray.hpp>
#include <rtt/TaskContext.hpp>
#include <rtt/Property.hpp>
#include <rtt/Port.hpp>
#include <Eigen/Core>

namespace trajectory_generators {

class tdFromForceGenerator: public RTT::TaskContext {
public:
	tdFromForceGenerator(const std::string& name);
	virtual ~tdFromForceGenerator() {};

	virtual bool configureHook();
	virtual bool startHook();
	virtual void updateHook();
	virtual void stopHook();
	virtual void cleanupHook() {};
        bool checkJoints();
	int findJointIndex(const std::string& string, const std::vector<std::string>& table);
	void arrangeJoints();


private:
	RTT::OutputPort<KDL::Frame> pos_des;
	RTT::OutputPort<KDL::Twist> vel_des;

	// error on joint velocities from joint controllers from root to <segment frame>
	RTT::InputPort<std::vector<double> > deltaqdot_port;
	//Jq=RelJacobian=cfr.RelTwist(ee|ee,b,b) (ref.point ee on object ee expressed in b)
	RTT::InputPort<KDL::Jacobian> Jq_port;
	RTT::OutputPort<std::vector<double> > Jqvec_port;
        ///Input: All joint names 
        RTT::InputPort<std::vector<std::string> > joint_names_port;

	KDL::Jacobian Jq, Jq_select;
	KDL::JntArray e_kdl;
	std::vector<double> e_std;
	KDL::Frame pos_des_local;
	KDL::Twist vel_des_local;

	std::vector<std::string> names_o1, names_o2, joint_names, chain_joints, chain_joints_alph, available_joints, joints_force_feedback, joints_force_feedback_alph;

	double x,y,z, omegax, omegay, omegaz;
	double tau, tau_prop, alpha;
	double nq;
	bool filter, foundFlag;

	RTT::os::TimeService::ticks previous_time;
	RTT::os::TimeService::Seconds delta_time;

	std::vector<double> Jqvec;

        ///metadata
        std::string metadata;


};//class
}//namespace

