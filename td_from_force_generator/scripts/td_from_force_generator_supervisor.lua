require "rttlib"
require"rfsm"
require "rfsm_emem" --needed for event memory
require "rfsm_rtt"
require "rfsm_ext"  --needed for the sequential-AND state 
require "rfsmpp"    --needed for the sequential-AND state
require "kdlpp"     --kdl pritty print (should be included in lua path!)
require "rttros"    --needed for rttros.find_rospack()

-- tc=Task Context of the compent we are in ( in this case SuperVisor)
tc=rtt.getTC()
local common_events_in, priority_events_in
local timer_id_in_fs ,timer_id_in
local depl

function configureHook()  
    local peers = tc:getPeers()
    for i,val in pairs(peers) do
      if val =="Deployer" then
        depl = tc:getPeer("Deployer")
        break
      elseif val=="Deployer" then
        depl = tc:getPeer("Deployer")
        break
      end
    end

    ComponentName =  string.gsub(tc:getName(),"_supervisor","") -- this removes the "_supervisor" part in the name
    DriverEventName = tc:getName() -- can be written over by the driver_event_name property
    componentPeer =  depl:getPeer(ComponentName)
	
    -- PROPERTIES
    -- create a timer id property
        application_timer_id=rtt.Property("int", "application_timer_id", "Timer ID of the application timer")
        tc:addProperty(application_timer_id)
    -- create a FSM property
        td_from_force_generator_fsm_package_prop=rtt.Property("string","td_from_force_generator_fsm_package","package where to find the td_from_force_generator_fsm, if any")
        tc:addProperty(td_from_force_generator_fsm_package_prop)
	td_from_force_generator_fsm_prop=rtt.Property("string", "td_from_force_generator_fsm", "path and name to the FSM file of the td_from_force_generator, starting from the package (start with a slash), if any")
	tc:addProperty(td_from_force_generator_fsm_prop)
    -- create properties to connect the ports
        robotName = rtt.Property("string","robotName","string with the name of the robot")
        tc:addProperty(robotName)
        o1Name = rtt.Property("string","o1Name","string with the name of object frame 1")
        tc:addProperty(o1Name)
        o2Name = rtt.Property("string","o2Name","string with the name of object frame 2")
        tc:addProperty(o2Name)
        ofName = rtt.Property("string","ofName","string with the name of the 'longest' object frame")
        tc:addProperty(ofName)
    -- fill in standard values for the properties (this will cause it to work as in previous versions)
       application_timer_id:set(1)
       td_from_force_generator_fsm_package_prop:set("")
       td_from_force_generator_fsm_prop:set(rttros.find_rospack("td_from_force_generator") .. "/scripts/td_from_force_generator_fsm.lua")

    -- INPUT PORTS 
	-- Port to recieve trigger from a timer
	time_trigger = rtt.InputPort("int")
	tc:addEventPort(time_trigger,"trigger","Port to recieve trigger from a timer")

	-- the following creates a string input port, adds it as a 
	-- port to the Taskcontext. The third line generates a
	-- getevents function, which returns all data on the current port as
	-- events. This function is called by the rFSM core to check for
	-- new events.
	common_events_in = rtt.InputPort("string")
	tc:addPort(common_events_in, "common_events_in", "rFSM common_event input port")

	-- the following creates a string input port, adds it as an event
	-- driven port to the Taskcontext. The third line generates a
	-- getevents function, which returns all data on the current port as
	-- events. This function is called by the rFSM core to check for
	-- new events. (usefull for eg. e_stop event)
	priority_events_in = rtt.InputPort("string")
	tc:addEventPort(priority_events_in, "priority_events_in", "rFSM priority_event input port")

	-- TRIGGER events: the following creates a string input port, adds it as an event
	-- driven port to the Taskcontext. 
	trigger_events_in = rtt.InputPort("string")
	tc:addEventPort(trigger_events_in, "trigger_events_in", "trigger_event input port")

	-- OUTPUT PORTS

	-- create a string port with which the current common events are send
	common_events_out = rtt.OutputPort("string")
	tc:addPort(common_events_out, "common_events_out", "current common events in FSM")

	-- create an event driven string port with which the current priority events are send
	priority_events_out = rtt.OutputPort("string")
	tc:addPort(priority_events_out, "priority_events_out", "current priority events in FSM")

	-- create a string port with which the current TRIGGER events are send
	trigger_events_out = rtt.OutputPort("string")
	tc:addPort(trigger_events_out, "trigger_events_out", "current trigger_events in FSM")
        print("td_from_force_supervisor is configured!")
	return true	
end

function startHook()
    rtt.logl("Info","[td_from_force_generator_supervisor.lua] starting") 

    -- getting the file locations 
    if(td_from_force_generator_fsm_package_prop:get()=="")
    then
        rtt.logl("Info","[force_trajectory_generator_supervisor.lua] No force_trajectory_generator_fsm_package specified, will look for fsm file on location specified by force_trajectory_generator_fsm property")
        td_from_force_generator_fsm_file = td_from_force_generator_fsm_prop:get()
    else
        td_from_force_generator_fsm_file = rttros.find_rospack(td_from_force_generator_fsm_package_prop:get()) .. td_from_force_generator_fsm_prop:get()
    end

	-- FSM
	-- load state machine
	fsm = rfsm.init(rfsm.load(td_from_force_generator_fsm_file))
      --    rfsm.pre_step_hook_add(fsm, function(fsm, events)
      --if #events > 1 then print("generator_supervisor received: "..utils.tab2str(events)) end
      --    end)
	-- get all events from the all input ports
	fsm.getevents = rfsm_rtt.gen_read_events(common_events_in, priority_events_in, trigger_events_in)


	-- optional: create a string port to which the currently active
	-- state of the FSM will be written. gen_write_fqn generates a
	-- function suitable to be added to the rFSM step hook to do this.
	fqn_out = rtt.OutputPort("string")
	tc:addPort(fqn_out, "currentState", "current active rFSM state")
	rfsm.post_step_hook_add(fsm,rfsm_rtt.gen_write_fqn(fqn_out))

	--raise event functions
	raise_common_event=rfsm_rtt.gen_raise_event(common_events_out, fsm)
	raise_priority_event=rfsm_rtt.gen_raise_event(priority_events_out, fsm)	
	raise_trigger_event=rfsm_rtt.gen_raise_event(trigger_events_out, fsm)

        return true
end
	
timer_id_in = rtt.Variable('int')
function updateHook() 
	timer_id_in_fs = time_trigger:read(timer_id_in)

	--TODO in updatehook event lezen dat functie connectports triggert.

	--check whether this component is triggered by a timer and if so, whether it is the correct timer
	if timer_id_in_fs=="NewData" then
		if timer_id_in:tolua()==application_timer_id:get() then
			 rfsm.run(fsm)
		 end
	else 
		rfsm.run(fsm)
	end
end

function cleanupHook()
    rttlib.tc_cleanup()
end

-- connect the robot's ports
function connect_ports_and_set_properties()
	-- lua:
    if not depl then
      rtt.logl("Error","[force_trajectory_generator_supervisor.lua] ERROR: force_trajectory_generator has no peer Deployer or deployer")
	  raise_common_event("e_emergency")  
    end
    
        -- determine which objecframe is the "longest" (has to happen in supervisor, since in normal script, the robot is not configured yet and therefor, these properties dont exist yet)
        robot_sup = depl:getPeer(robotName:get())

	names_o1 = rtt.Variable("strings")
	names_o2 = rtt.Variable("strings")
	names_o1 = robot_sup:getProperty("jnt_names_"..o1Name:get().."_base_prop"):get()
	names_o2 = robot_sup:getProperty("jnt_names_"..o2Name:get().."_base_prop"):get()

	local objectFrame

	if (names_o1.size>names_o2.size) then
	  objectFrame = o1Name:get()
	else 
	  objectFrame = o2Name:get()
	end

	-- ROS connection policy
	roscp=rtt.Variable("ConnPolicy")
	roscp.transport = 3
        -- Connection policy
        local cp = rtt.Variable('ConnPolicy')
	
        -- connect the Jacobian port of pr2robot to the trajectory generators
        depl:connect(ComponentName..".Jq_w", robotName:get()..".Jq_refpee_"..objectFrame.."_base", cp)        
        -- connect the velocity errors
        depl:connect(ComponentName..".deltaqdot", robotName:get()..".deltaqdot_"..objectFrame.."_base", cp)

        -- set properties (names-ports of the robot have to be send using properties, since there they are created during configuring, but when they can be connected by the supervisor, the data is already written and there is no possibility to rewrite the data again after connection)
	componentPeer:getProperty("names_o1"):set(names_o1)
	componentPeer:getProperty("names_o2"):set(names_o2)
end
