local rttlib = require("rttlib")
local rttros = require("rttros")
local strict = require("strict")

local force_trajectory_generator={}
local robotName
function force_trajectory_generator.deploy(config)

-- default settings
local period = 0.01 -- timer period, only used if timer is started here (no other timer present)
local TimerName = "Timer"
local TdFromForceGeneratorSupervisorName = "supervisor"
local PackageName = "td_from_force_generator"
local SupervisorPath = "scripts/td_from_force_generator_supervisor.lua"
local ApplicationSupervisorName = "application_supervisor"
local TdFromForceGeneratorSupervisorProp = rttros.find_rospack("td_from_force_generator") .. "/cpf/td_from_force_generator_supervisor.cpf"

-- override default settings with config
local TdFromForceGeneratorName

if config then
  if not config.stand_alone then
    if config.application_supervisor then
      ApplicationSupervisorName = config.application_supervisor
    end
    if config.timer then
      TimerName = config.timer
    end
    if config.prefix then
      TdFromForceGeneratorName = config.prefix
      TdFromForceGeneratorSupervisorName = config.prefix.."_"..TdFromForceGeneratorSupervisorName
    end
    if config.config then
      TdFromForceGeneratorSupervisorProp = config.config
    end
  else
    rtt.logl("Warning","Running force trajectory generator stand alone, using default parameters")
  end
else
  rtt.logl("Error","No configuration for the force trajectory generator deployment given, will use default parameters")
end

--
local timer_present = true
local tc = rtt.getTC()
local depl
if "Deployer" == tc:getName() then
  depl = tc
else
  depl = tc:getPeer("Deployer")
end

-- timer
local tcPeers = depl:getPeers()
local timer = false
for i,k in pairs(tcPeers) do
  if k == TimerName then
    timer = depl:getPeer(TimerName)
  end
end
if not timer then
  timer_present = false
  depl:loadComponent(TimerName,"OCL::TimerComponent")
  timer = depl:getPeer(TimerName)
  depl:setActivity(TimerName, 0, 0, rtt.globals.ORO_SCHED_RT)
end

-- import packages
rtt.logl("Info","Importing force_trajectory_generator packages")
depl:import("ocl")
depl:import("kdl_typekit")
depl:import("rtt_rosnode")
depl:import("rtt_tf")
depl:import("td_from_force_generator")

--load components
rtt.logl("Info","creating force_trajectory_generator components")
depl:loadComponent(TdFromForceGeneratorSupervisorName, "OCL::LuaComponent")
local td_from_force_generator_supervisor = depl:getPeer(TdFromForceGeneratorSupervisorName)

-- set activity
rtt.logl("Info","set activity of force_trajectory_generator components")

-- add peers
td_from_force_generator_supervisor:addPeer(depl)

-- load td_from_force_generator FSM
rtt.logl("Info","loading and configuring force_trajectory_generator FSM's")
local res, packpath = pcall(rttros.find_rospack, PackageName)
if not res then
  rtt.logl("Error","Could not find package "..PackageName.." containing td_from_force_generator")
end
td_from_force_generator_supervisor:exec_file(packpath.."/"..SupervisorPath)
if td_from_force_generator_supervisor:configure() then
  setProperties(config,depl,td_from_force_generator_supervisor)
  rtt.logl("Info", TdFromForceGeneratorSupervisorName.. " configured")
else
  rtt.logl("Error", "Unable to configure "..TdFromForceGeneratorSupervisorName)
end

-- load properties
rtt.logl("Info","loading properties")
local UpdateProps
if TdFromForceGeneratorSupervisorProp then
  depl:loadService( TdFromForceGeneratorSupervisorName, "marshalling")
  UpdateProps = td_from_force_generator_supervisor:provides("marshalling"):getOperation("updateProperties")
  UpdateProps(TdFromForceGeneratorSupervisorProp)
end

-- connect ports
rtt.logl("Info","Connecting force_trajectory_generator ports")
local cp = rtt.Variable('ConnPolicy')
local roscp = rtt.Variable('ConnPolicy')
roscp.transport = 3

local application_supervisor = false
for i,j in pairs (tcPeers) do
  if j == ApplicationSupervisorName then
    application_supervisor = depl:getPeer(j)
  end
end
depl:connect(TdFromForceGeneratorSupervisorName..".trigger", TimerName..".timeout", cp)
if application_supervisor then
  depl:connect(ApplicationSupervisorName..".application_priority_events_out", TdFromForceGeneratorSupervisorName..".priority_events_in", cp)
  depl:connect(ApplicationSupervisorName..".application_priority_events_in", TdFromForceGeneratorSupervisorName..".priority_events_out", cp)
  depl:connect(ApplicationSupervisorName..".application_trigger_events_out", TdFromForceGeneratorSupervisorName..".trigger_events_in", cp)
  depl:connect(ApplicationSupervisorName..".application_trigger_events_in", TdFromForceGeneratorSupervisorName..".trigger_events_out", cp)
  depl:connect(ApplicationSupervisorName..".application_common_events_out", TdFromForceGeneratorSupervisorName..".common_events_in", cp)
  depl:connect(ApplicationSupervisorName..".application_common_events_in", TdFromForceGeneratorSupervisorName..".common_events_out", cp)
else
  rtt.logl("Warning","Unable to find "..ApplicationSupervisorName.." and unable connect to it")
end

rtt.logl("Info","Connecting forcetrajectory component ports")
depl:connect(TdFromForceGeneratorName..".joint_names", robotName..".joint_names", cp)

-- configure and start components
if not timer_present then
  timer:configure()
  timer:start()
  timer:startTimer(td_from_force_generator_supervisor:getProperty("application_timer_id"):get(), period)
end

td_from_force_generator_supervisor:start()

local setup={}
setup.supervisor=TdFromForceGeneratorSupervisorName
return setup
end

function setProperties(config,depl,force_trajectory_generator_supervisor)
local trajectoryGeneratorName
local trajectoryGeneratorIndex
local taskIndex
local o1
local o2
local robot_sup

--STEP 1: Search for the correct force trajectory generator
for i,k in ipairs(model.setpoint_generators) do
  if(model.setpoint_generators[i].name == config.prefix) then
    trajectoryGeneratorIndex = i
    trajectoryGeneratorName = model.setpoint_generators[trajectoryGeneratorIndex].name
  end
end

--STEP 2: Search for the task which controller connects with this trajectory generator
for i,k in ipairs(model.itasc.tasks) do
  if model.itasc.tasks[i].connect ~= nil then
    for j,l in ipairs(model.itasc.tasks[i].connect) do
      if(model.itasc.tasks[i].connect[j]._srcref.name == trajectoryGeneratorName) then
        taskIndex = i
      end
      if(model.itasc.tasks[i].connect[j]._tgtref.name == trajectoryGeneratorName) then
        taskIndex = i
      end
    end
  end
end

--STEP 3: Get robot name
local i
i = string.find(model.itasc.tasks[taskIndex].vkc.o1,"%.")
robotName = string.sub(model.itasc.tasks[taskIndex].vkc.o1, 1, i-1)

--STEP 4: Get both o1 and o2 of the corresponding task
o1 = string.gsub(model.itasc.tasks[taskIndex].vkc.o1,robotName..".","")
o2 = string.gsub(model.itasc.tasks[taskIndex].vkc.o2,robotName..".","")

--STEP 5: Set properties to transfer to supervisor
force_trajectory_generator_supervisor:getProperty("robotName"):set(robotName)
force_trajectory_generator_supervisor:getProperty("o1Name"):set(o1)
force_trajectory_generator_supervisor:getProperty("o2Name"):set(o2)

end

return force_trajectory_generator

