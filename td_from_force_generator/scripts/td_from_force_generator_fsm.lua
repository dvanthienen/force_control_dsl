-- force trajectory generator FSM
-- require("ansicolors")

return rfsm.composite_state{
	--dbg = fsmpp.gen_dbgcolor3({["STATE_ENTER"]=true, ["STATE_EXIT"]=true, ["HIBERNATING"]=false, ["EXEC_PATH"]=true, ["EFFECT"]=false, ["DOO"]=false, ["CHECKING"]=false, ["RAISED"]=true},  false,   ansicolors.yellow .. ansicolors.bright ..  "APPLICATIONfsm" ..  ansicolors.reset),

NONemergency = rfsm.composite_state{
    PreOperational = rfsm.simple_state{},

	Configuring = rfsm.simple_state{
		entry=function()
			print("=>td from force generator FSM->Configuring state entry")
		
            	end,

	},
	
	Configured = rfsm.simple_state{
		entry=function()
			print("=>td from force generator FSM->Configured state entry")
			raise_common_event("e_"..ComponentName.."Configured")
			raise_common_event("e_"..DriverEventName.."_configured")
		end,
	},
	
	Starting = rfsm.simple_state{
		entry=function()
			print("=>td from force generator FSM->Starting state")
                        connect_ports_and_set_properties()
		end,
	},
	
	Started = rfsm.simple_state{
		entry=function()
			print("=>td from force generator FSM->Started state entry")
			raise_common_event("e_"..ComponentName.."Started")
			raise_common_event("e_"..DriverEventName.."_started")
		end,
	},
	
	Running = rfsm.simple_state{
		entry=function()
			print("=>td from force generator FSM->Running state")
		end,
	},
	
	Stopping = rfsm.simple_state{
		entry=function()
			print("=>itd from force generator FSM->Stopping state")
		end,
	},
	
	Stopped = rfsm.simple_state{
		entry=function()
			print("=>td from force generator FSM->Stopped state")
			raise_common_event("e_"..ComponentName.."Stopped")
			raise_common_event("e_"..DriverEventName.."_stopped")
		end,
	},
	
    rfsm.transition { src='initial', tgt='PreOperational' },
	rfsm.transition { src='PreOperational', tgt='Configuring', events={ 'e_configure_trajectoryGenerators' } },
	rfsm.transition { src='Configuring', tgt='Configured', events={ 'e_done' } },
	rfsm.transition { src='Configuring', tgt='Stopping', events={ 'e_stop_trajectoryGenerators' } },
	rfsm.transition { src='Configured', tgt='Starting', events={ 'e_start_trajectoryGenerators' } },
	rfsm.transition { src='Configured', tgt='Stopping', events={ 'e_stop_trajectoryGenerators' } },
	rfsm.transition { src='Starting', tgt='Started', events={ 'e_done' } },
	rfsm.transition { src='Starting', tgt='Stopping', events={ 'e_stop_trajectoryGenerators' } },
	rfsm.transition { src='Started', tgt='Running', events={ 'e_run_trajectoryGenerators' } },
	rfsm.transition { src='Started', tgt='Stopping', events={ 'e_stop_trajectoryGenerators' } },
	rfsm.transition { src='Running', tgt='Stopping', events={ 'e_stop_trajectoryGenerators' } },
	rfsm.transition { src='Stopping', tgt='Stopped', events={ 'e_done' } },
},

Emergency = rfsm.simple_state{
	entry = function ()
		raise_priority_event("e_emergency")
		print("[EMERGENCY] => application in emergency state!")
		print("[EMERGENCY] =>force trajector generator FSM has therefore raised a priority emergency event")
	end,
},

rfsm.transition { src='initial', tgt='NONemergency' },
rfsm.transition { src='NONemergency', tgt='Emergency', events={'e_emergency'} },

}

