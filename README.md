Force control DSL
=================

This repository contains different packages that contribute to perform force control in the iTaSC DSL framework:

**force_sensorless_applications**: This package contains the M1 level model to execute a task in which force control is used to make contact with a table.  It consists of the iTaSC DSL model, the necessary scripts to run this model and to define the composite task and cpf-files for the configuration of this demo.
  
**desvel_from_force_generator**: This package contains the trajectory generator used in *force\_sensorless\_applications* to perform force sensorless force control. The trajectory generator consists of three components: desvel\_from\_force\_generator, force\_offset\_generator and td\_from\_force\_generator.  
  
**force_offset_generator**: This package consists of an OROCOS component to calculate the correct feedforward signal that converts the desired force into a desired velocity for the iTaSC framework.  
  
**td\_from\_force\_generator**: This package consists of an OROCOS component to calculate the feedback signal that determines the dynamical behaviour of the force control. It is based on the cartesian velocity error. Further, it also contains the scripts to run this component as a seperate trajectory generator, which is used in the bimanual comanipulation demo.

Dependencies
------------

* iTaSC DSL installation

Setup
-----

Follow the setup explained by the itasc\_dsl.

Run the demo
------------

Follow the PR2 start-up procedure to start the robot

### 1. Run the velocity controllers
In a first terminal, run the proportional velocity controllers on the pr2:
```sh
$ roscd itasc_comanipulation_demo/itasc_comanipulation_demo_app
$ ./runControllers.sh
```

### 2. Run the model
In a second terminal, run the model using the runfile, for example:
```sh
$ roscd force_sensorless_applications/scripts
$ ./run.sh
```

This starts up the demo. Except for the degree of freedom that is force controlled, all other degrees of freedom are now position controlled. 

### 3. Choose the desired force
To set the desired force execute following commands in the deployer
```sh
Deployer [S]> cd force_trajectory_generator_forceOffsetGenerator
force_trajectory_generator_forceOffsetGenerator [R]> leave
force_trajectory_generator_forceOffsetGenerator [R]> Fd.write(-10)
```
This will make the end effector move downwards. When it reaches the table, a force of 10 N will be executed. In free space, the velocity that corresponds with the desired force will depend on the configuration of the robot arm.

Configuration
-------------

Different parameters of the demo can be configured by changing them in the corresponding cpf files that the models refers to.

Assumptions/simplifications in the current implementation
------------------------------------------

* The force control is based on a control scheme that assumes that the joints contributing to the force are all backdrivable and controlled by proportional low level velocity controllers.
* The gain adaption factor Cf, is considered a scalar (the same value in all force controlled directions).
* The the force offset generator does not use the pseudo-inverse of the augmented jacobian, assuming no damping was applied (far from singularities), and no conflicting constraints with the force controlled directions (no weighting or priority effects)
* S=Sc (the force controlled directions are the directions with contact that will be taken into account)
* Fd selection, not projection, hence: FF*S*Fd not FF*Fd

