my_tasks = {
   Task{
      name= "apply_force_in_Z",
      vkc = VKC{type="iTaSC::VKC_Cartesian", package="cartesian_motion",
                o1="pr2.torso_lift_link", o2="pr2.l_gripper_wiper_frame_tip"},
      cc = CC{type="iTaSC::CC_Cartesian", package="cartesian_motion",
              prio_num=1, config={"file://force_sensorless_applications#/cpf/CC_Cartesian_force.cpf"} }, 
      connect = {
        Rewire{src="force_trajectory_generator", tgt="cc",
           {from="all",to="all"}
        }
      },
      fsm = FSM{fsm = "file://cartesian_motion#/scripts/cartesian_motion_supervisor.lua"}
   },

   Task{
      name= "apply_position_control_in_5DOF",
      vkc = VKC{type="iTaSC::VKC_Cartesian", package="cartesian_motion",
                o1="pr2.torso_lift_link", o2="pr2.l_gripper_wiper_frame_tip"},
      cc = CC{type="iTaSC::CC_Cartesian", package="cartesian_motion",
              prio_num=1, config={"file://force_sensorless_applications#/cpf/CC_Cartesian_position.cpf"} },
      connect = {
        Rewire{src="wipe_generator", tgt="cc",
           {from="all",to="all"}
        }
      },
      fsm = FSM{fsm = "file://cartesian_motion#/scripts/cartesian_motion_supervisor.lua"}
   },
 
   Task{
      name= "hold_standard_pose",
      cc = CC{type="iTaSC::CC_PDFFjoints", package="joint_motion",
              prio_num=2, config={"file://force_sensorless_applications#/cpf/CC_PDFFjoints_pose.cpf"} },
      robot = "pr2",
      connect = {
        Rewire{src="naxis_generator", tgt="cc",
           {from="all",to="all"}
        }
      },
      fsm = FSM{fsm = "file://joint_motion#/scripts/joint_motion_supervisor.lua"}
   }
}
-- end of tasks

return Application {
   dsl_version = '0.1',
   name = 'app_force_sensorless_contact_with_table',
   uri = 'be.kuleuven.mech.robotics.application.force_sensorless_contact_with_table',
   fsm = FSM{fsm = "file://itasc_core#/scripts/reduced_application_supervisor.lua"},
   
   -- itasc
   itasc = iTaSC {
      dsl_version = '0.1',
      name = 'force_sensorless_contact_with_table',
      uri = 'be.kuleuven.mech.robotics.itasc.force_sensorless_contact_with_table',
      fsm = FSM{fsm = "file://itasc_core#/scripts/reduced_itasc_supervisor.lua",
                config = {"file://force_sensorless_applications#/cpf/itasc_wipe_supervisor.cpf"}},
      -- robots
      robots = {
        Robot{
                name = "pr2",
                package = "itasc_pr2",
                type = "iTaSC::pr2Robot",
            driver = "pr2_driver",
            config = {"file://itasc_pr2#/cpf/pr2robot.cpf"}
            },
      },
   
      -- scene
      scene = {
             SceneElement {
                robot = "pr2",
                location = Frame{
                   M = Rotation{X_x=1,Y_x=0,Z_x=0,X_y=0,Y_y=1,Z_y=0,X_z=0,Y_z=0,Z_z=1},
                   p = Vector{X=0.0,Y=0.0,Z=0.0}
                },
             },          
      },
      tasks = my_tasks,
   
      solver = Solver{ name="Solver", package="diagw_dls_prior_vel_solver", type="iTaSC::DiagWDLSPriorVelSolver", 
		config={"file://force_sensorless_applications#/cpf/Solver.cpf"} }
   
   }, --end of iTaSC

   setpoint_generators = {
      SetpointGenerator{name="force_trajectory_generator",
                        type="trajectory_generators::Desvel_from_force_generator",
                        package="desvel_from_force_generator",
                        file="file://desvel_from_force_generator#/scripts/force_trajectory_generator.lua",
                        config={"file://force_sensorless_applications#/cpf/force_trajectory_generator.cpf"}
      },
      SetpointGenerator{name="wipe_generator",
                        type="trajectory_generators::LissajousGenerator",
                        package="lissajous_generator",
                        config={"file://force_sensorless_applications#/cpf/wipe_generator.cpf"}
      },
      SetpointGenerator{name="naxis_generator",
                        type="trajectory_generators::nAxesGeneratorPos",
                        package="naxes_joint_generator",
                        config = {{
				-- {baseRotZ, baseTransX, baseTransY, head_pan_joint, head_tilt_joint, l_elbow_flex_joint, l_forearm_roll_joint, l_shoulder_lift_joint, l_shoulder_pan_joint, l_upper_arm_roll_joint, l_wrist_flex_joint, l_wrist_roll_joint, r_elbow_flex_joint, ..., torso_lift_joint}
--                                 constraint_value = {0, 0, 0, 0, 0, -1.16755,    7.46962,     -0.391005,   0.419683,    1.98652,     -1.25682,    3.8, 0, 0, 0, 0, 0, 0, 0, 0}, 
                                 constraint_value = {0, 0, 0, 0, 0, -1.62227,    7.60527,     -0.344985,   0.42275,     2.00657,     -1.12912,    3.58, 0, 0, 0, 0, 0, 0, 0, 0}, 
                                 --constraint_value = {0, 0, 0, 0, 0, -1.24022    8.27635     0.0963465   0.439663    1.06304     -1.93974    3.89472 hh-1.1, 1.24, -0.22, 0.48, 2.0, -1.22, 2.53, 0, 0, 0, 0, 0, 0, 0, 0}, 
                                 execution_time = {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
                                 maximum_velocity = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
                                 maximum_acceleration = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2}
                                 }}
      }
   }, --end of setpoint_generators

   drivers = {
      Driver{name="pr2_driver", package="itasc_pr2", file="file://itasc_pr2#/scripts/pr2driver.lua"}
   }
} --end of application

