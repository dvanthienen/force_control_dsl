my_tasks = {
   Task{
      name= "apply_force_in_Y",
      vkc = VKC{type="iTaSC::VKC_Cartesian", package="cartesian_motion",
                o1="pr2.torso_lift_link", o2="pr2.l_gripper_tool_frame_tip"},
      cc = CC{type="iTaSC::CC_Cartesian", package="cartesian_motion",
              prio_num=1, config={"file://force_sensorless_applications#/cpf/CC_Cartesian_force_y.cpf"} }, 
      connect = {
        Rewire{src="force_trajectory_generator", tgt="cc",
           {from="all",to="all"}
        }
      },
      fsm = FSM{fsm = "file://cartesian_motion#/scripts/cartesian_motion_supervisor.lua"}
   },

   Task{
      name= "apply_position_control_in_5DOF",
      vkc = VKC{type="iTaSC::VKC_Cartesian", package="cartesian_motion",
                o1="pr2.torso_lift_link", o2="pr2.l_gripper_tool_frame_tip"},
      cc = CC{type="iTaSC::CC_Cartesian", package="cartesian_motion",
              prio_num=1, config={"file://force_sensorless_applications#/cpf/CC_Cartesian_position_yfree.cpf"} },
      connect = {
        Rewire{src="fixed_pose_generator", tgt="cc",
           {from="all",to="all"}
        }
      },
      fsm = FSM{fsm = "file://cartesian_motion#/scripts/cartesian_motion_supervisor.lua"}
   },
 
   Task{
      name= "hold_standard_pose",
      cc = CC{type="iTaSC::CC_PDFFjoints", package="joint_motion",
              prio_num=2, config={"file://force_sensorless_applications#/cpf/CC_PDFFjoints_pose.cpf"} },
      robot = "pr2",
      connect = {
        Rewire{src="naxis_generator", tgt="cc",
           {from="all",to="all"}
        }
      },
      fsm = FSM{fsm = "file://joint_motion#/scripts/joint_motion_supervisor.lua"}
   }
}
-- end of tasks

return Application {
   dsl_version = '0.1',
   name = 'app_force_sensorless_contact_with_table',
   uri = 'be.kuleuven.mech.robotics.application.force_sensorless_contact_with_table',
   fsm = FSM{fsm = "file://itasc_core#/scripts/reduced_application_supervisor.lua"},
   
   -- itasc
   itasc = iTaSC {
      dsl_version = '0.1',
      name = 'force_sensorless_contact_with_table',
      uri = 'be.kuleuven.mech.robotics.itasc.force_sensorless_contact_with_table',
      fsm = FSM{fsm = "file://itasc_core#/scripts/reduced_itasc_supervisor.lua",
                config = {"file://force_sensorless_applications#/cpf/itasc_supervisor_init_table_contact3.cpf"}},
      -- robots
      robots = {
        Robot{
                name = "pr2",
                package = "itasc_pr2",
                type = "iTaSC::pr2Robot",
            driver = "pr2_driver",
            config = {"file://force_sensorless_applications#/cpf/pr2robot.cpf"}
            },
      },
   
      -- scene
      scene = {
             SceneElement {
                robot = "pr2",
                location = Frame{
                   M = Rotation{X_x=1,Y_x=0,Z_x=0,X_y=0,Y_y=1,Z_y=0,X_z=0,Y_z=0,Z_z=1},
                   p = Vector{X=0.0,Y=0.0,Z=0.0}
                },
             },          
      },
      tasks = my_tasks,
   
      solver = Solver{ name="Solver", package="diagw_dls_prior_vel_solver", type="iTaSC::DiagWDLSPriorVelSolver",
		config={"file://force_sensorless_applications#/cpf/Solver.cpf"} }
   
   }, --end of iTaSC

   setpoint_generators = {
      SetpointGenerator{name="force_trajectory_generator",
                        type="trajectory_generators::Desvel_from_force_generator",
                        package="desvel_from_force_generator",
                        file="file://desvel_from_force_generator#/scripts/force_trajectory_generator.lua",
                        config={"file://force_sensorless_applications#/cpf/force_trajectory_generator_y.cpf"}
      },
      SetpointGenerator{name="fixed_pose_generator",
                        type="trajectory_generators::FixedPoseGenerator",
                        package="fixed_pose_generator",
			config={"file://force_sensorless_applications#/cpf/fixed_pose_generator_y.cpf"}
      },
      SetpointGenerator{name="naxis_generator",
                        type="trajectory_generators::nAxesGeneratorPos",
                        package="naxes_joint_generator",
                        config = {{
                                 --{"TransZ","RotZ","TransX","RotX","RotY","RotZ"}
				 --y
                                 --constraint_value = {0, 0, 0, 0, 0, -1.1952,     0.589456,    0.713297,    0.94929,     1.54555,     -1.22567,    3.38445, 0, 0, 0, 0, 0, 0, 0, 0}, --old
--				 constraint_value = { 0,0,0,0,0, -1.64, 0.59, 0.36, 0.77, 1.5, -0.74, -0.197,0, 0, 0, 0, 0,0, 0, 0}, --elbow almost horizontal
				 constraint_value = { 0,0,0,0,0, -1.61, 1.04, 0.74, 0.61, 1.0, -1.07, -0.53, 0, 0, 0, 0, 0,0, 0, 0}, --elbow more down
                                 --z 
--				                 constraint_value = {0, 0, 0, 0, 0, -1.53, 1.50, -0.04, 0.61, 1.26, -1.82, 2.20, 0, 0, 0, 0, 0, 0, 0, 0}, 
                                 execution_time = {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
                                 maximum_velocity = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
                                 maximum_acceleration = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2}
                                 }}
      }
   }, --end of setpoint_generators

   drivers = {
      Driver{name="pr2_driver", package="itasc_pr2", file="file://itasc_pr2#/scripts/pr2driver.lua"}
   }
} --end of application

