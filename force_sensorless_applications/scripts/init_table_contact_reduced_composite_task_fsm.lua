-- iTaSC composite task fsm

require "rfsm_ext"
require "rfsmpp"
require "rfsm_timeevent"
rfsm_timeevent.set_gettime_hook(rtt.getTime)

local state, trans = rfsm.state, rfsm.transition
local depl, depla, fgen, Fdport
Fd=rtt.Variable("float64[]")
Fd:fromtab{0,-10,-10,0,0,0} --not used directions will be deselected using Svec
Wptot=rtt.Variable("float64[]")
Wptot:fromtab{1,0.3,0.3,1,1,1}

-- the FSM
return state {
    dbg=rfsmpp.gen_dbgcolor("composite_task_fsm.lua", { STATE_ENTER=true, STATE_EXIT=true }, false),

    SavePose = state{  -- The pose of the trajectory has to be set before the tasks are started, so a previous state was necessary
        entry = function()
	    Reporter:configure()            
            print("--- EXECUTE savePose ---")
            --raise_common_event('e_fixed_pose_generator_savePose') --use current pose (or part thereof)
            raise_common_event('e_fixed_pose_generator_poseSaved') --use pose of property
        end,
    },

    JointTasksActive = state{
        entry = function()
            print("--- MOVE TO DESIRED 2ND JOINT POSITIONS ---")
            raise_common_event('e_hold_standard_pose_LimitJointPositions')
	    raise_common_event('e_naxis_generator_moveToGoal')
--            raise_common_event('e_all_tasks_activate')
--            raise_common_event('e_all_trajectory_generators_moveToGoal')  --the force_trajectory_generator does not use this event. It starts automatically, the input is zero. The input is changed manually afterwards in the deployer
        end,
    },
        
    AllTasksActive = state{
        entry = function()
            print("--- POSE TASK AND 2ND TASKS ACTIVE ---")
            raise_common_event('e_hold_standard_pose_LimitJointPositions')
            raise_common_event('e_all_tasks_activate')
            raise_common_event('e_all_trajectory_generators_moveToGoal')  --the force_trajectory_generator does not use this event. It starts automatically, the input is zero. The input is changed manually afterwards in the deployer
        end,
    },

    ApplyForce = state{
	entry = function()
		Reporter:start()
		print("--- APPLY FORCE ---")
		depl = tc:getPeer("Reporter")
		fgen = depl:getPeer("force_trajectory_generator_forceOffsetGenerator")
		Fdport = rttlib.port_clone_conn(fgen:getPort("Fd"))
		Fdport:write(Fd)
	end,
    },

    endstep = state{
	entry = function()
			print("--- ALL FORCE STEPS APPLIED ---")
			Reporter:stop()
			depl = tc:getPeer("Reporter")
			raise_common_event('e_apply_force_in_Z_DoNothing')
			ccpos=depl:getPeer('CC_apply_position_control_in_5DOF')
			ccpos:getProperty("W"):set(Wptot)
	end,
    },

    NoTasksActive = state{
        entry = function()
            raise_common_event('e_all_tasks_deactivate')
        end,
    },

    trans{ src='initial', tgt='SavePose' },
    trans{ src='SavePose', tgt='JointTasksActive', events={ "e_fixed_pose_generator_poseSaved" }},
    --trans{ src='SavePose', tgt='AllTasksActive', events={ "e_fixed_pose_generator_poseSaved" }},
    trans{ src='JointTasksActive', tgt='AllTasksActive', events={ "naxis_generator_move_finished" }},
    trans{ src='AllTasksActive', tgt='ApplyForce', events={ "e_force" }},
    trans{ src='NoTasksActive', tgt='AllTasksActive', events={ "e_start_all_tasks" }},
    --trans{ src='ApplyForce', tgt='endstep', events={ "e_after(5)" }},

    trans{ src='AllTasksActive', tgt='NoTasksActive', events={ "e_stop" }},
    trans{ src='AllTasksActive', tgt='NoTasksActive', events={ "e_emergency" }},
    trans{ src='JointTasksActive', tgt='NoTasksActive', events={ "e_stop" }},
    trans{ src='JointTasksActive', tgt='NoTasksActive', events={ "e_emergency" }},
    trans{ src='ApplyForce', tgt='NoTasksActive', events={ "e_emergency" }},
    trans{ src='ApplyForce', tgt='NoTasksActive', events={ "e_stop" }},
    trans{ src='endstep', tgt='NoTasksActive', events={ "e_emergency" }},
    trans{ src='endstep', tgt='NoTasksActive', events={ "e_stop" }},
}

