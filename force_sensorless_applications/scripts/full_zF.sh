#!/bin/bash
export OROCOS_TARGET=gnulinux
rosrun ocl deployer-gnulinux -lwarning -s `rospack find itasc_dsl`/itasc_deploy_taskbrowser.ops -- -r file://force_sensorless_applications#/cpf/reporter.cpf file://force_sensorless_applications#/models/make_contact_with_table_full_z.lua
