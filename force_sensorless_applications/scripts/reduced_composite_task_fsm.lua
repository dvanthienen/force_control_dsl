-- iTaSC composite task fsm

require "rfsm_ext"
require "rfsmpp"
require "rfsm_timeevent"
rfsm_timeevent.set_gettime_hook(rtt.getTime)

local state, trans = rfsm.state, rfsm.transition
local depl, depla, fgen, Fdport

-- the FSM
return state {
    dbg=rfsmpp.gen_dbgcolor("composite_task_fsm.lua", { STATE_ENTER=true, STATE_EXIT=true }, false),

    SavePose = state{  -- The pose of the trajectory has to be set before the tasks are started, so a previous state was necessary
        entry = function()
	    Reporter:configure()            
            print("--- EXECUTE savePose ---")
            raise_common_event('e_fixed_pose_generator_savePose')
        end,
    },
        
    AllTasksActive = state{
        entry = function()
            print("--- ACTIVATE ALL TASKS AND MOVE TO ALL GOALS ---")
--            raise_common_event('e_hold_standard_pose_LimitJointPositions')
            raise_common_event('e_all_tasks_activate')
            raise_common_event('e_all_trajectory_generators_moveToGoal')  --the force_trajectory_generator does not use this event. It starts automatically, the input is zero. The input is changed manually afterwards in the deployer
        end,
    },

    ApplyForce = state{
	entry = function()
		Reporter:start()
		print("--- APPLY FORCE ---")
		depl = tc:getPeer("Reporter")
		fgen = depl:getPeer("force_trajectory_generator_forceOffsetGenerator")
		Fdport = rttlib.port_clone_conn(fgen:getPort("Fd"))
		Fdport:write(-10)
	end,
    },

    NoTasksActive = state{
        entry = function()
            raise_common_event('e_all_tasks_deactivate')
        end,
    },

    trans{ src='initial', tgt='SavePose' },
    trans{ src='SavePose', tgt='AllTasksActive', events={ "e_fixed_pose_generator_poseSaved" }},
    trans{ src='AllTasksActive', tgt='NoTasksActive', events={ "e_stop" }},
    trans{ src='AllTasksActive', tgt='NoTasksActive', events={ "e_emergency" }},
    trans{ src='AllTasksActive', tgt='ApplyForce', events={ "e_force" }},
    trans{ src='ApplyForce', tgt='NoTasksActive', events={ "e_emergency" }},
    trans{ src='ApplyForce', tgt='NoTasksActive', events={ "e_stop" }},
    trans{ src='NoTasksActive', tgt='AllTasksActive', events={ "e_start_all_tasks" }},
}

