-- iTaSC composite task fsm

require "rfsm_ext"
require "rfsmpp"
require "rfsm_timeevent"
rfsm_timeevent.set_gettime_hook(rtt.getTime)

local state, trans = rfsm.state, rfsm.transition
local wipe_generator = false
local depl, depla, fgen, Fdport
Fd=rtt.Variable("float64[]")
Fd:fromtab{0,0,-10,0,0,0} --not used directions will be deselected using Svec

-- the FSM
return state {
    dbg=rfsmpp.gen_dbgcolor("composite_task_fsm.lua", { STATE_ENTER=true, STATE_EXIT=true }, false),

    SavePose = state{  -- The pose of the trajectory has to be set before the tasks are started, so a previous state was necessary
        entry = function()
	    Reporter:configure() 
	wipe_generator=tc:getPeer("wipe_generator")
	wipe_generator:stop()
        end,
    },

    JointTasksActive = state{
        entry = function()
            print("--- MOVE TO DESIRED 2ND JOINT POSITIONS ---")
            raise_common_event('e_hold_standard_pose_LimitJointPositions')
	    raise_common_event('e_naxis_generator_moveToGoal')
        end,
    },
        
    AllTasksActive = state{
        entry = function()
		wipe_generator:start()
            print("--- ACTIVATE ALL TASKS AND MOVE TO ALL GOALS ---")
--            raise_common_event('e_hold_standard_pose_LimitJointPositions')
            raise_common_event('e_all_tasks_activate')
            raise_common_event('e_all_trajectory_generators_moveToGoal')  --the force_trajectory_generator does not use this event. It starts automatically, the input is zero. The input is changed manually afterwards in the deployer. Also the lissajous generator is not started (cpf changed eventname)
        end,
    },

    ApplyForce = state{
	entry = function()
		Reporter:start()
		print("--- APPLY FORCE ---")
		depl = tc:getPeer("Reporter")
		fgen = depl:getPeer("force_trajectory_generator_forceOffsetGenerator")
		Fdport = rttlib.port_clone_conn(fgen:getPort("Fd"))
		Fdport:write(Fd)
	end,
    },
        
    Wipe = state{
        entry = function()
            print("--- start wiping ---")
            raise_common_event('e_lissajous_generator_move')
        end,
    },

    NoTasksActive = state{
        entry = function()
            raise_common_event('e_all_tasks_deactivate')
        end,
    },

    trans{ src='initial', tgt='SavePose' },
    trans{ src='SavePose', tgt='JointTasksActive', events={ "e_fixed_pose_generator_poseSaved" }},
    --trans{ src='SavePose', tgt='AllTasksActive', events={ "e_after(1)" }},
    trans{ src='AllTasksActive', tgt='ApplyForce', events={ "e_force" }},
    trans{ src='ApplyForce', tgt='Wipe', events={ "e_wipe" }},
    trans{ src='Wipe', tgt='NoTasksActive', events={ "e_emergency" }},
    trans{ src='Wipe', tgt='NoTasksActive', events={ "e_stop" }},
    trans{ src='ApplyForce', tgt='NoTasksActive', events={ "e_emergency" }},
    trans{ src='ApplyForce', tgt='NoTasksActive', events={ "e_stop" }},
    trans{ src='AllTasksActive', tgt='NoTasksActive', events={ "e_stop" }},
    trans{ src='AllTasksActive', tgt='NoTasksActive', events={ "e_emergency" }},
    trans{ src='NoTasksActive', tgt='AllTasksActive', events={ "e_start_all_tasks" }},
}

