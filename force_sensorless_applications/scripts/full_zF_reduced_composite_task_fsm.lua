-- iTaSC composite task fsm
-- change in endstep force stop event to correct direction!

require "rfsm_ext"
require "rfsmpp"
require "rfsm_timeevent"
rfsm_timeevent.set_gettime_hook(rtt.getTime)

local state, trans = rfsm.state, rfsm.transition
local depl, depla, fgen, Fdport
Fda=rtt.Variable("float64[]")
Fda:fromtab{0,0,-2,0,0,0} --not used directions will be deselected using Svec
Fdb=rtt.Variable("float64[]")
Fdb:fromtab{0,0,-5,0,0,0} --not used directions will be deselected using Svec
Fdc=rtt.Variable("float64[]")
Fdc:fromtab{0,0,-10,0,0,0} --not used directions will be deselected using Svec
Fdd=rtt.Variable("float64[]")
Fdd:fromtab{0,0,-15,0,0,0} --not used directions will be deselected using Svec
Wptot=rtt.Variable("float64[]")
Wptot:fromtab{1,1,0.7,1,1,1}
--Wptot:fromtab{1,1,0.1,1,1,1} --z

-- the FSM
return state {
    dbg=rfsmpp.gen_dbgcolor("composite_task_fsm.lua", { STATE_ENTER=true, STATE_EXIT=true }, false),

    SavePose = state{  -- The pose of the trajectory has to be set before the tasks are started, so a previous state was necessary
        entry = function()
	    Reporter:configure()            
            print("--- EXECUTE savePose ---")
            --raise_common_event('e_fixed_pose_generator_savePose') --use current pose (or part thereof)
            raise_common_event('e_fixed_pose_generator_poseSaved') --use pose of property
        end,
    },
        
    AllTasksActive = state{
        entry = function()
            print("--- MOVE TO DESIRED 2ND JOINT POSITIONS ---")
            raise_common_event('e_hold_standard_pose_LimitJointPositions')
            raise_common_event('e_all_tasks_activate')
            raise_common_event('e_all_trajectory_generators_moveToGoal')  --the force_trajectory_generator does not use this event. It starts automatically, the input is zero. The input is changed manually afterwards in the deployer
        end,
    },

    ApplyForce = state{
	startstep = state{
		entry = function()
			--disable position control task
			raise_common_event('e_apply_position_control_in_5DOF_DoNothing')
		end,
	},
	stepA = state{
		entry = function()
			Reporter:start()
			print("--- APPLY FORCE A---")
			depl = tc:getPeer("Reporter")
			fgen = depl:getPeer("force_trajectory_generator_forceOffsetGenerator")
			Fdport = rttlib.port_clone_conn(fgen:getPort("Fd"))
			Fdport:write(Fda)
		end,
	}, 
	stepB = state{
		entry = function()
			print("--- APPLY FORCE B---")
			depl = tc:getPeer("Reporter")
			fgen = depl:getPeer("force_trajectory_generator_forceOffsetGenerator")
			Fdport = rttlib.port_clone_conn(fgen:getPort("Fd"))
			Fdport:write(Fdb)
		end,
	}, 
	stepC = state{
		entry = function()
			print("--- APPLY FORCE C---")
			depl = tc:getPeer("Reporter")
			fgen = depl:getPeer("force_trajectory_generator_forceOffsetGenerator")
			Fdport = rttlib.port_clone_conn(fgen:getPort("Fd"))
			Fdport:write(Fdc)
		end,
	}, 
	stepD = state{
		entry = function()
			print("--- APPLY FORCE D---")
			depl = tc:getPeer("Reporter")
			fgen = depl:getPeer("force_trajectory_generator_forceOffsetGenerator")
			Fdport = rttlib.port_clone_conn(fgen:getPort("Fd"))
			Fdport:write(Fdd)
		end,
	}, 
	endstep = state{
		entry = function()
			print("--- ALL FORCE STEPS APPLIED ---")
			Reporter:stop()
			depl = tc:getPeer("Reporter")
			--disable F control
			raise_common_event('e_apply_force_in_Z_DoNothing')
			--enable position control
			raise_common_event('e_appy_position_control_in_5DOF_CartesianMotionOn')
			ccpos=depl:getPeer('CC_apply_position_control_in_5DOF')
			ccpos:getProperty("W"):set(Wptot)
		end,
	},
    	trans{ src='initial', tgt='startstep' },
    	--trans{ src='stepA', tgt='stepB', events={ "e_after(10)" }},
    	trans{ src='startstep', tgt='stepB', events={ "e_after(1)" }},
    	trans{ src='stepB', tgt='stepC', events={ "e_after(10)" }},
    	trans{ src='stepC', tgt='stepD', events={ "e_after(5)" }},
    	trans{ src='stepD', tgt='endstep', events={ "e_after(5)" }},
    },

    NoTasksActive = state{
        entry = function()
            raise_common_event('e_all_tasks_deactivate')
        end,
    },

    trans{ src='initial', tgt='SavePose' },
    trans{ src='SavePose', tgt='AllTasksActive', events={ "e_fixed_pose_generator_poseSaved" }},
    trans{ src='AllTasksActive', tgt='NoTasksActive', events={ "e_stop" }},
    trans{ src='AllTasksActive', tgt='NoTasksActive', events={ "e_emergency" }},
    trans{ src='AllTasksActive', tgt='ApplyForce', events={ "e_force" }},
    trans{ src='ApplyForce', tgt='NoTasksActive', events={ "e_emergency" }},
    trans{ src='ApplyForce', tgt='NoTasksActive', events={ "e_stop" }},
    trans{ src='NoTasksActive', tgt='AllTasksActive', events={ "e_start_all_tasks" }},
}

