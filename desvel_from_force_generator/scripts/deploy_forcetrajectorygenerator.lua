local rttlib = require("rttlib")
local rttros = require("rttros")
local strict = require("strict")

driver_file = dofile(rttros.find_rospack("desvel_from_force_generator").."/scripts/force_trajectory_generator.lua")
driver_conf = {}
driver_conf.stand_alone = true
local setup = driver_file.deploy(driver_conf)
local tc = rtt.getTC()
local depl
if "Deployer" == tc:getName() then
  depl = tc
else
  depl = tc:getPeer("Deployer")
end
local driver_sup = depl:getPeer(setup.supervisor)
local common_events_out = rttlib.port_clone_conn(driver_sup:getPort("common_events_in"))
local common_events_in = rttlib.port_clone_conn(driver_sup:getPort("common_events_out"))
common_events_out:write("e_configure_trajectory_generators")
local driver_started = false
local event_in
local watchdog = 0
local timeout = 50
local fs = "NewData"
local driver_started_failed = false
while not driver_started do
   fs = "NewData"
   while fs=="NewData" do
     fs, event_in = common_events_in:read()
     if event_in=="e_force_trajectory_generator_supervisor_configured" then
      driver_started = true
     end
   end
 watchdog = watchdog + 1
 if watchdog > timeout then
   driver_started = true
   driver_started_failed = true
 end
 rtt.sleep(0.5, 0)
end
if driver_started_failed then
  rtt.logl("Error","deploy force trajectory generator failed: watchdog time-out: didn't receive trajectory generator configured event")
else
  common_events_out:write("e_start_trajectory_generators")
end

