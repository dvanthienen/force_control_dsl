local rttlib = require("rttlib")
local rttros = require("rttros")
local strict = require("strict")

local force_trajectory_generator={}
local robotName
function force_trajectory_generator.deploy(config)

-- default settings
local period = 0.01 -- timer period, only used if timer is started here (no other timer present)
local ForceOffsetGeneratorNameDft = "forceOffsetGenerator"
local TdFromForceGeneratorNameDft = "tdFromForceGenerator"
local DesvelFromForceGeneratorNameDft = ""
local TimerName = "Timer"
local ForceTrajectoryGeneratorSupervisorName = "supervisor"
local PackageName = "desvel_from_force_generator"
local SupervisorPath = "scripts/force_trajectory_generator_supervisor.lua"
local ApplicationSupervisorName = "application_supervisor"
local ForceTrajectoryGeneratorSupervisorProp = rttros.find_rospack("desvel_from_force_generator") .. "/cpf/force_trajectory_generator_supervisor.cpf"
local ForceTrajectoryGeneratorProp = rttros.find_rospack("force_sensorless_applications") .. "/cpf/force_trajectory_generator.cpf"
local TdFromForceGeneratorProp = rttros.find_rospack("force_sensorless_applications") .. "/cpf/force_trajectory_generator.cpf"

-- override default settings with config
local ForceOffsetGeneratorName
local TdFromForceGeneratorName
local DesvelFromForceGeneratorName
local ReporterName

if config then
  if not config.stand_alone then
    if config.application_supervisor then
      ApplicationSupervisorName = config.application_supervisor
    end
    if config.timer then
      TimerName = config.timer
    end
    if config.prefix then
      ForceOffsetGeneratorName = config.prefix.."_"..ForceOffsetGeneratorNameDft
      TdFromForceGeneratorName = config.prefix.."_"..TdFromForceGeneratorNameDft
      DesvelFromForceGeneratorName = config.prefix
      ForceTrajectoryGeneratorSupervisorName = config.prefix.."_"..ForceTrajectoryGeneratorSupervisorName
    end
    if config.config then
      ForceTrajectoryGeneratorSupervisorProp = config.config
      ForceTrajectoryGeneratorProp = config.config
      TdFromForceGeneratorProp = config.config
      rtt.logl("Info",ForceTrajectoryGeneratorSupervisorName..", "..ForceOffsetGeneratorName ..", and "..TdFromForceGeneratorName .. " properties will be updated with "..ForceTrajectoryGeneratorSupervisorProp)
    end
    if config.reporter then
      ReporterName = config.reporter
    end
  else
    rtt.logl("Warning","Running force trajectory generator stand alone, using default parameters")
  end
else
  rtt.logl("Error","No configuration for the force trajectory generator deployment given, will use default parameters")
end

--
local timer_present = true
local tc = rtt.getTC()
local depl
if "Deployer" == tc:getName() then
  depl = tc
else
  depl = tc:getPeer("Deployer")
end

-- timer
local tcPeers = depl:getPeers()
local timer = false
for i,k in pairs(tcPeers) do
  if k == TimerName then
    timer = depl:getPeer(TimerName)
  end
end
if not timer then
  timer_present = false
  depl:loadComponent(TimerName,"OCL::TimerComponent")
  timer = depl:getPeer(TimerName)
  depl:setActivity(TimerName, 0, 0, rtt.globals.ORO_SCHED_RT)
end

-- import packages
rtt.logl("Info","Importing force_trajectory_generator packages")
depl:import("ocl")
depl:import("kdl_typekit")
depl:import("rtt_rosnode")
depl:import("rtt_tf")
depl:import("td_from_force_generator")
depl:import("force_offset_generator")
depl:import("desvel_from_force_generator")

--load components
rtt.logl("Info","creating force_trajectory_generator components")
depl:loadComponent(ForceOffsetGeneratorName, "Force_offset_generator")
depl:loadComponent(TdFromForceGeneratorName, "trajectory_generators::tdFromForceGenerator")
depl:loadComponent(ForceTrajectoryGeneratorSupervisorName, "OCL::LuaComponent")
local force_trajectory_generator_supervisor = depl:getPeer(ForceTrajectoryGeneratorSupervisorName)
local forceoffsetgenerator = depl:getPeer(ForceOffsetGeneratorName)
local tdfromforcegenerator = depl:getPeer(TdFromForceGeneratorName)
local desvelfromforcegenerator = depl:getPeer(DesvelFromForceGeneratorName)

-- set activity
rtt.logl("Info","set activity of force_trajectory_generator components")
depl:setActivity(ForceOffsetGeneratorName, 0.0, 99, rtt.globals.ORO_SCHED_RT)
depl:setActivity(TdFromForceGeneratorName, 0.0, 99, rtt.globals.ORO_SCHED_RT)
depl:setActivity(ForceTrajectoryGeneratorSupervisorName, 0.0, 99, rtt.globals.ORO_SCHED_OTHER)

-- add peers
force_trajectory_generator_supervisor:addPeer(depl)
force_trajectory_generator_supervisor:addPeer(forceoffsetgenerator)

-- load force_trajectory_generator FSM
rtt.logl("Info","loading and configuring force_trajectory_generator FSM's")
local res, packpath = pcall(rttros.find_rospack, PackageName)
if not res then
  rtt.logl("Error","Could not find package "..PackageName.." containing force_trajectory_generator")
end
force_trajectory_generator_supervisor:exec_file(packpath.."/"..SupervisorPath)
if force_trajectory_generator_supervisor:configure() then
  setProperties(config,depl,force_trajectory_generator_supervisor)
  rtt.logl("Info", ForceTrajectoryGeneratorSupervisorName.. " configured")
else
  rtt.logl("Error", "Unable to configure "..ForceTrajectoryGeneratorSupervisorName)
end

-- load properties
rtt.logl("Info","loading properties")
local UpdateProps
if ForceTrajectoryGeneratorSupervisorProp then
  depl:loadService( ForceTrajectoryGeneratorSupervisorName, "marshalling")
  UpdateProps = force_trajectory_generator_supervisor:provides("marshalling"):getOperation("updateProperties")
  UpdateProps(ForceTrajectoryGeneratorSupervisorProp)
end
if ForceTrajectoryGeneratorProp then
  depl:loadService(ForceOffsetGeneratorName, "marshalling")
  UpdateProps = forceoffsetgenerator:provides("marshalling"):getOperation("updateProperties")
  UpdateProps(ForceTrajectoryGeneratorProp)
end
if TdFromForceGeneratorProp then
  depl:loadService(TdFromForceGeneratorName, "marshalling")
  UpdateProps = tdfromforcegenerator:provides("marshalling"):getOperation("updateProperties")
  UpdateProps(TdFromForceGeneratorProp)
end

-- Reporter
local tcPeers = depl:getPeers()
local reporter= false
if config.reporter then
for i,k in pairs(tcPeers) do
  if k == ReporterName then
    reporter = depl:getPeer(ReporterName)
    reporter:addPeer(tdfromforcegenerator)
    reporter:addPeer(forceoffsetgenerator)
  end
end
end

-- connect ports
rtt.logl("Info","Connecting force_trajectory_generator ports")
local cp = rtt.Variable('ConnPolicy')
local roscp = rtt.Variable('ConnPolicy')
roscp.transport = 3

local application_supervisor = false
for i,j in pairs (tcPeers) do
  if j == ApplicationSupervisorName then
    application_supervisor = depl:getPeer(j)
  end
end
depl:connect(ForceTrajectoryGeneratorSupervisorName..".trigger", TimerName..".timeout", cp)
if application_supervisor then
  depl:connect(ApplicationSupervisorName..".application_priority_events_out", ForceTrajectoryGeneratorSupervisorName..".priority_events_in", cp)
  depl:connect(ApplicationSupervisorName..".application_priority_events_in", ForceTrajectoryGeneratorSupervisorName..".priority_events_out", cp)
  depl:connect(ApplicationSupervisorName..".application_trigger_events_out", ForceTrajectoryGeneratorSupervisorName..".trigger_events_in", cp)
  depl:connect(ApplicationSupervisorName..".application_trigger_events_in", ForceTrajectoryGeneratorSupervisorName..".trigger_events_out", cp)
  depl:connect(ApplicationSupervisorName..".application_common_events_out", ForceTrajectoryGeneratorSupervisorName..".common_events_in", cp)
  depl:connect(ApplicationSupervisorName..".application_common_events_in", ForceTrajectoryGeneratorSupervisorName..".common_events_out", cp)
else
  rtt.logl("Warning","Unable to find "..ApplicationSupervisorName.." and unable connect to it")
end

rtt.logl("Info","Connecting forcetrajectory component ports")
depl:connect(ForceOffsetGeneratorName..".tdf", DesvelFromForceGeneratorName..".yex", cp)
depl:connect(TdFromForceGeneratorName..".DesiredVelocity", DesvelFromForceGeneratorName..".vel_error", cp)
depl:connect(ForceOffsetGeneratorName..".joint_names", robotName..".joint_names", cp)
depl:connect(TdFromForceGeneratorName..".joint_names", robotName..".joint_names", cp)

-- configure and start components
if not timer_present then
  timer:configure()
  timer:start()
  timer:startTimer(force_trajectory_generator_supervisor:getProperty("application_timer_id"):get(), period)
end

force_trajectory_generator_supervisor:start()

local setup={}
setup.supervisor=ForceTrajectoryGeneratorSupervisorName
return setup
end

function setProperties(config,depl,force_trajectory_generator_supervisor)
local trajectoryGeneratorName
local trajectoryGeneratorIndex
local taskIndex
local o1
local o2
local robot_sup

--STEP 1: Search for the correct force trajectory generator
for i,k in ipairs(model.setpoint_generators) do
  if(model.setpoint_generators[i].name == config.prefix) then
    trajectoryGeneratorIndex = i
    trajectoryGeneratorName = model.setpoint_generators[trajectoryGeneratorIndex].name
  end
end

--STEP 2: Search for the task which controller connects with this trajectory generator
for i,k in ipairs(model.itasc.tasks) do
  for j,l in ipairs(model.itasc.tasks[i].connect) do
    if(model.itasc.tasks[i].connect[j]._srcref.name == trajectoryGeneratorName) then
      taskIndex = i
    end
    if(model.itasc.tasks[i].connect[j]._tgtref.name == trajectoryGeneratorName) then
      taskIndex = i
    end
  end
end

--STEP 3: Get robot name
local i
i = string.find(model.itasc.tasks[taskIndex].vkc.o1,"%.")
robotName = string.sub(model.itasc.tasks[taskIndex].vkc.o1, 1, i-1)

--STEP 4: Get both o1 and o2 of the corresponding task
o1 = string.gsub(model.itasc.tasks[taskIndex].vkc.o1,robotName..".","")
o2 = string.gsub(model.itasc.tasks[taskIndex].vkc.o2,robotName..".","")

--STEP 5: Set properties to transfer to supervisor
force_trajectory_generator_supervisor:getProperty("robotName"):set(robotName)
force_trajectory_generator_supervisor:getProperty("o1Name"):set(o1)
force_trajectory_generator_supervisor:getProperty("o2Name"):set(o2)

end

return force_trajectory_generator
