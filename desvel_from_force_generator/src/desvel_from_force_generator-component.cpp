#include "desvel_from_force_generator-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>

namespace trajectory_generators
{

Desvel_from_force_generator::Desvel_from_force_generator(std::string const& name) : TaskContext(name),
  S(6,0),
  Cf(0),
  flagA(false),
  flagB(false),
  metadata("{ports={"
   "DesiredVelocity={type='trajectory_generator', geom_sem='TwistCoordinateSemantics(o2,o2,o1,o1)'},"
  "}}")

  {

  this->addProperty("Cf",Cf).doc("Feedback gain for force control");
  this->addProperty("Svec",S).doc("Vector selecting the DOF used force control, can only contain 6 elements, which are 0 or 1");
  this->addProperty("metadata",metadata).doc("Meta-data of the component");

  this->ports()->addEventPort("yex",yex_port,boost::bind(&Desvel_from_force_generator::setflagA, this) );
  this->ports()->addEventPort("vel_error", vel_error_port,boost::bind(&Desvel_from_force_generator::setflagB, this) );
  this->ports()->addPort("DesiredVelocity", td_port);

  yex.resize(6);
  yex.setZero(6);

  std::cout << "Desvel_from_force_generator constructed !" <<std::endl;
}

bool Desvel_from_force_generator::configureHook(){
  std::cout << "Desvel_from_force_generator configured !" <<std::endl;
  return true;
}

bool Desvel_from_force_generator::startHook(){
  std::cout << "Desvel_from_force_generator started !" <<std::endl;
  return true;
}

void Desvel_from_force_generator::updateHook(){

  if (flagA && flagB){

  yex_port.read(yex);
  for(unsigned int i=0; i<6; i++){//twist has always 6 elements
	tex(i)=yex(i);
  }
  vel_error_port.read(vel_error);
  tcf = Cf*vel_error;
  td = tex + tcf;
  //tdsel = S.asDiagonal()*td;//does not work: twist has no eigen data structure underneath
  SetToZero(tdsel);
  for(unsigned int i=0;i<6; i++){//twist has always 6 elements
	if(S[i]!=0){//S should contain only 0/1 elements
	  tdsel(i)=td(i);
	}
  }

  td_port.write(tdsel);
  
  flagA = false;
  flagB = false;
  }

  //std::cout << "Desvel_from_force_generator executes updateHook !" <<std::endl;
}

void Desvel_from_force_generator::stopHook() {
  std::cout << "Desvel_from_force_generator executes stopping !" <<std::endl;
}

void Desvel_from_force_generator::cleanupHook() {
  std::cout << "Desvel_from_force_generator cleaning up !" <<std::endl;
}

void Desvel_from_force_generator::setflagA(){
  flagA = true;
}

void Desvel_from_force_generator::setflagB(){
  flagB = true;
}
} //end of namespace

/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Desvel_from_force_generator)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(trajectory_generators::Desvel_from_force_generator)
