#ifndef OROCOS_DESVEL_FROM_FORCE_GENERATOR_COMPONENT_HPP
#define OROCOS_DESVEL_FROM_FORCE_GENERATOR_COMPONENT_HPP

#include <rtt/RTT.hpp>
#include <rtt/TaskContext.hpp>
#include <rtt/Port.hpp>
#include <rtt/Operation.hpp>
#include <std_msgs/Float64.h>
#include <kdl/kdl.hpp>
#include <kdl/frames.hpp>
#include <kdl/jntarray.hpp>
#include <kdl/jacobian.hpp>
#include <Eigen/Core>
#include <vector>
#include <rtt/RTT.hpp>

namespace trajectory_generators
{

using namespace KDL;
using namespace RTT;
using namespace Eigen;
using namespace std;

class Desvel_from_force_generator : public RTT::TaskContext{
  public:
    Desvel_from_force_generator(std::string const& name);
    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();

    void setflagA();
    void setflagB();
 
  private: 
    ///Input: Velocity offset
    RTT::InputPort<Eigen::VectorXd> yex_port; 
    ///Input: Velocity error feedback
    RTT::InputPort<KDL::Twist> vel_error_port;
    ///Output: Desired velocity
    RTT::OutputPort<KDL::Twist> td_port;

    //variables
    Eigen::VectorXd yex;
    KDL::Twist vel_error;
    KDL::Twist td, tdsel, tcf, tex;
    std::vector<int> S;//int to avoid accurracy errors

    double Cf;
    ///synchronization
    bool flagA;
    bool flagB;  
    ///metadata
    std::string metadata;

};

} // end of namespace
#endif
