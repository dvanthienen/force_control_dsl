#ifndef OROCOS_FORCE_OFFSET_GENERATOR_COMPONENT_HPP
#define OROCOS_FORCE_OFFSET_GENERATOR_COMPONENT_HPP

#include <rtt/RTT.hpp>
#include <rtt/TaskContext.hpp>
#include <rtt/Port.hpp>
#include <rtt/Operation.hpp>
#include <std_msgs/Float64.h>
#include <kdl/kdl.hpp>
#include <kdl/frames.hpp>
#include <kdl/jntarray.hpp>
#include <kdl/jacobian.hpp>
#include <Eigen/Core>
#include <vector>
#include <rtt/RTT.hpp>

using namespace KDL;
using namespace RTT;
using namespace Eigen;
using namespace std;

class Force_offset_generator : public RTT::TaskContext{
  public:
    Force_offset_generator(std::string const& name);
    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();

    bool checkJoints();
    int findJointIndex(const std::string& string, const std::vector<std::string>& table);
    void arrangeJoints();

  private:
    ///Input: Desired force
    RTT::InputPort< std::vector<double> > Fd_port;
    ///Input: Jacobian of the chain from base to contact point
    RTT::InputPort<KDL::Jacobian> Jq_port;
    ///Output: Desired velocity offset to produce desired force
    RTT::OutputPort<Eigen::VectorXd> tdf_port;

    ///Input: All joint names 
    RTT::InputPort<std::vector<std::string> > joint_names_port;

    //Variables
    std::vector<double> Fd;
    double Cf;
    double nq;
    std::vector<int> S;//int to avoid precision errors

    Eigen::VectorXd Fd_vec, yex_vec;
    Eigen::VectorXd Kv;
    KDL::Jacobian Jq, Jq_select;
    Eigen::MatrixXd Kvinv, Jq_T_select, Id, FF, FF2, FF3;
    double FF1;

    std::vector<std::string> names_o1, names_o2, joint_names, chain_joints, chain_joints_alph, available_joints;

    // flag
    bool flagFirstUpdateHook, foundFlag;
 };
#endif
