#include "force_offset_generator-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>

//TODO veralgemeen de controle-input van 1 DOF naar 6 DOF, momenteel hardcoded krachtcontrole volgens z-as
Force_offset_generator::Force_offset_generator(std::string const& name) : TaskContext(name),
  Fd(6,0.0),
  Cf(0),
  S(6,0),
  Kv(Matrix<double, 7, 1>::Zero()),
  FF1(0)
  {

  // Properties
  this->addProperty("Cf",Cf).doc("Feedback gain for force control");
  this->addProperty("Svec",S).doc("Index selecting the DOF used force control");
  this->addProperty("Kv",Kv).doc("Proportional Velocity Gain of the low level velocity controllers");
  this->addProperty("FF",FF).doc("Feedforward gain");
  this->addProperty("available_joints",available_joints);
  this->addProperty("names_o1",names_o1);
  this->addProperty("names_o2",names_o2);
  this->addProperty("joint_names",joint_names);
  this->addProperty("chain_joints",chain_joints);
  this->addProperty("chain_joints_alph",chain_joints_alph);

  // Ports
  this->ports()->addPort("Fd",Fd_port).doc("Desired force");
  this->ports()->addEventPort("Jq",Jq_port).doc("Jacobian of the chain");
  this->ports()->addPort("tdf",tdf_port).doc("desired velocity offset");
  this->ports()->addPort("joint_names",joint_names_port).doc("all available joint names");

  std::cout << "Force_offset_generator constructed !" <<std::endl;
}

bool Force_offset_generator::configureHook(){
  Logger::In in(this->getName());

  Id.resize(6,6);
  Id.setIdentity();
//  Cf_matrix.resize(6,6);
//  Cf_matrix.setZero();
  Fd_vec.resize(6,1);
  yex_vec.resize(6,1);
  Fd_vec.setZero();
  yex_vec.setZero();
  //FF1.resize(6,6);
  //FF1.setZero();
  FF.resize(6,6);
  FF.setZero();

  joint_names_port.read(joint_names);

  // Allocate safe amount of memory
  Jq_select.data.resize(6,joint_names.size());
  Jq_select.data.setZero();
  Kvinv.resize(joint_names.size(),joint_names.size());
  Kvinv.setZero();

  std::cout << "Force_offset_generator configured !" <<std::endl;
  return true;
}

bool Force_offset_generator::startHook(){
  flagFirstUpdateHook = true;

  joint_names_port.read(joint_names);
  checkJoints();
  arrangeJoints();  std::cout << "Force_offset_generator started !" <<std::endl;

  // Allocate correct amount of memory
  Kvinv.resize(nq,nq);
  Kvinv.setZero();
  Jq_select.data.resize(6,nq);
  Jq_select.data.setZero();
  FF2.resize(6,nq);
  FF2.setZero();
  FF3.resize(nq,6);
  FF3.setZero();
 
  return true;
}

void Force_offset_generator::updateHook(){
  if (!flagFirstUpdateHook) {
 
    //Set Kv inverse
    for (unsigned int i = 0; i < nq; i++)
    {
    	Kvinv(i,i) = 1.0/Kv(findJointIndex(chain_joints_alph[i], available_joints));
    }

    // Set J_q_T
    Jq_port.read(Jq);
    for (int index = 0; index<nq; index++){
    Jq_select.setColumn(index,Jq.getColumn(findJointIndex(chain_joints_alph[index], joint_names)));
    }
    Jq_T_select = Jq_select.data.transpose();

    // Set Cf-matrix=S*pinv(S)*cf: left out here, since desvel_from_force will do the selection
    //Cf_matrix(force_index,force_index) = Cf;

    // FF = (Id-Cf)*J_q.data*(Kvinv*J_q_T)
    //FF1.noalias() = Id - Cf_matrix;
    FF1 = 1-Cf;//always 1: assumption cf is a scalar!
    FF2.noalias() = FF1*Jq_select.data;
    FF3.noalias() = Kvinv*Jq_T_select;
    FF.noalias() = FF2*FF3;

    // CALCULATION OF THE DESIRED INPUT

    Fd_port.read(Fd);
    yex_vec.setZero();
    Fd_vec.setZero();
    //TODO: replace this by Fd from eigen vector type if the typekit/rttlua is updated to be able to handle this type
    for (unsigned int i = 0; i < 6; i++){
	if(S[i]!=0){//Fd must be selected to controlled directions, to assure only effect of desired wrench in those directions (otherwise combined effect due to matrix multiplication with FF!)
    	  //Fd_vec(force_index) = Fd;
	  Fd_vec(i)=Fd[i];
	}
    }
    yex_vec = FF*Fd_vec;

    tdf_port.write(yex_vec);
    }

  else {
    flagFirstUpdateHook = false;
  }
  //std::cout << "Force_offset_generator executes updateHook !" <<std::endl;
}

void Force_offset_generator::stopHook() {
  std::cout << "Force_offset_generator executes stopping !" <<std::endl;
}

void Force_offset_generator::cleanupHook() {
  std::cout << "Force_offset_generator cleaning up !" <<std::endl;
}

bool Force_offset_generator::checkJoints() {

  // Construct a vector with the joints used for force control,  by substracting both chains from eachother
  if (names_o1.size()>names_o2.size()){
    chain_joints.resize(names_o1.size()-names_o2.size());
    for (int i = 0; i<chain_joints.size(); i++){
      chain_joints[i] = names_o1[i+names_o2.size()];
    }
  }
  if (names_o1.size()<names_o2.size()){
    chain_joints.resize(names_o2.size()-names_o1.size());
    for (int i = 0; i<chain_joints.size(); i++){
      chain_joints[i] = names_o2[i+names_o1.size()];
    }
  }
  if (names_o1.size()==names_o2.size()){
    log(Error) << "No joints available for force control" << endlog();
  }

  // Check if the obtained joints are allowed to be used (backdrivable and proportional velocity control)
  for (int i = 0; i<chain_joints.size(); i++){
    foundFlag = false;
    for (int j = 0; j<available_joints.size(); j++){
      if (chain_joints[i] == available_joints[j]) {
	foundFlag = true;
      }
    }
    if (!foundFlag){
      log(Error) << "Joint "<<chain_joints[i]<<" cannot be used for force control" << endlog();
      return false;
    }
  }
  nq = chain_joints.size();
  return true;  
}

int Force_offset_generator:: findJointIndex(const std::string& string, const std::vector<std::string>& table) {
  unsigned int index;
  for (unsigned int i = 0; i<table.size(); i++) {
    if (string == table[i]){
      index = i;
      return index;
    }
  }
  log(Error) << "Joint is not present in the table" << endlog();
  return -1;
}

void Force_offset_generator:: arrangeJoints() {
  chain_joints_alph.resize(chain_joints.size());
  unsigned int k = 0;
  for (unsigned int i = 0; i<joint_names.size(); i++) {
    for (unsigned int j = 0; j<chain_joints.size(); j++) {
      if (chain_joints[j] == joint_names[i] ) {
        chain_joints_alph[k] = chain_joints[j];
        k++;
      }
    }
  }
}
        
/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Force_offset_generator)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(Force_offset_generator)
