
#
# Components, types and plugins.
#
# The CMake 'target' names are identical to the first argument of the
# macros below, except for orocos_typegen_headers, where the target is fully
# controlled by generated code of 'typegen'.
#

# Creates a component library libforce_offset_generator-<target>.so
# and installs in the directory lib/orocos/force_offset_generator/
#
orocos_component(force_offset_generator force_offset_generator-component.hpp force_offset_generator-component.cpp) # ...you may add multiple source files
#
# You may add multiple orocos_component statements.

#
# Additional headers:
#
# Installs in the include/orocos/force_offset_generator/ directory
#
orocos_install_headers(force_offset_generator-component.hpp) # ...you may add multiple header files
#
# You may add multiple orocos_install_headers statements.

#
# Building a Plugin
#
# Creates a plugin library libforce_offset_generator-plugin-<target>.so
# and installs in the directory lib/orocos/force_offset_generator/plugins/
#
# Be aware that a plugin may only have the loadRTTPlugin() function once defined in a .cpp file.
# This function is defined by the plugin and service CPP macros.
#
#orocos_plugin(force_offset_generator-plugin force_offset_generator-plugin.cpp) # ...only one plugin function per library !
#
# You may add multiple orocos_plugin statements.


#
# Building a Service:
#
# Creates a plugin library libforce_offset_generator-service-<target>.so
# and installs in the directory lib/orocos/force_offset_generator/plugins/
#
#orocos_service(force_offset_generator-service force_offset_generator-service.cpp) # ...only one service per library !
#
# You may add multiple orocos_service statements.


#
# Building a typekit using typegen (recommended):
#
# Creates a typekit library libforce_offset_generator-types-<target>.so
# and installs in the directory lib/orocos/@target@/force_offset_generator/types/
#
# The header will go in include/orocos/force_offset_generator/types/force_offset_generator/force_offset_generator-types.hpp
# So you can #include <force_offset_generator/force_offset_generator-types.hpp>
#
#orocos_typegen_headers(include/force_offset_generator/force_offset_generator-types.hpp) # ...you may add multiple header files
#
# You may only have *ONE* orocos_typegen_headers statement in your toplevel CMakeFile.txt !


#
# Building a normal library (optional):
#
# Creates a library libsupport-<target>.so and installs it in
# lib/
#
#orocos_library(support support.cpp) # ...you may add multiple source files
#
# You may add multiple orocos_library statements.


